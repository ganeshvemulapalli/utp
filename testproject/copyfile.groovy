import groovy.json.JsonOutput
import hudson.slaves.DumbSlave
import java.util.Date
import java.util.logging.Level
import org.jenkinsci.plugins.pipeline.utility.steps.fs.FileWrapper
import org.junit.runners.model.MultipleFailureException

List<String> nodeNames = new ArrayList<String>()
Long startTime = currentBuild.startTimeInMillis
Boolean success = true

Alert alert = new Alert(BUILD_NUMBER.toInteger(), BUILD_URL.toURL(), Level.WARNING, JOB_NAME.split('/').last(), this)

stage('Validation') { // Validate input parameters
	alert.info('Starting the Input Validation stage')
	try {
        if(!File_Name)         {throw new IllegalArgumentException('Filename cannot be null')}
    } catch(Exception ex) {
		alert.severe('Validation failed')
		throw ex
	}
}

stage('Check File') { // Checks if file actually exists
	def filepath = "C:/Java/jdk-10.0.2/README.html"
    def file = new File(filePath)
    if (file.exists()) EQ false {
		alert.severe("Could not find the file")
	}
   else {
     alert.info("File found")
    }
}

stage('Delete File') { // Deletes the file
    file.delete()
	}
